# influxdb-experiments

## Description
Analyze and monitor telemetry data with the telegraf, influx, grafana (TIG) stack using `docker-compose`.

### Concept of InfluxDB
InfluxDB is a database specialized to store and process time series data. Time series data is information that always comes with a timestamp. To process time series data efficiently, InfluxDB follows a concept that differs from relational databases.

#### Measurement
A *measurement* is similar to a table in relational databases. The *measurement* is represented as a column and describes in which logical entity a data is stored. A *measurement* consists of three types: *tags*, *fields* and *timestamps*. Names of a *measurement* are strings. An* InfluxDB instance can have multiple *measurements*.

#### Tag
*Tags* are similar to indexes in relational databases. Filter operations like `WHERE` or aggregation operagtions like `GROUP BY` can be performed on *tags*.
*Tags* consist of *keys* and *values* that are stored as string and metadata.  
Please take a look at [influx' documentation](https://docs.influxdata.com/influxdb/v2/reference/syntax/line-protocol/#tag-set) to understand the requirements corresponding to *tags*.

#### Field
*Fields* are the entities on which arithmetical operations can be performed, e.g., sum, mean.
A *field* consists of a *key* and a *value*. The *key* field represents the name of the field. The *value* represents the value of the associated field. *Keys* must be string. *Values* can be strings, floats, integers or booleans.

#### Timestamp
*Timestamp* represent the time of the recording of the corresponding data. The default time is represented in nanoseconds.
## Installation
Environment variables are managed using [direnv](#install-direnv). _docker-compose_ will consider the environment variables listed in the [_.envrc_](#customize-environment-variables) file.
[Here](#use-environment-variables) is shown how to make use of the environment varables by using _direnv_.

### Requirements
#### Installation of additional tools
- direnv - https://direnv.net/docs/hook.html

#### Prepare environment
##### Install direnv
- direnv - https://direnv.net/docs/hook.html

##### Customize environment variables
```sh
├── grafana/
├── telegraf/
├── .envrc             <---
├── docker-compose.yml
├── entrypoint.sh
└── ...
```

##### Use environment variables
execute direnv in the root directory:
```sh
direnv allow
```

### Run the TIG stack

#### Start
execute the following command in the root directory:
```sh
docker-compose up --force-recreate -V
```
Note: The command above will force the recreation of containers to and anonymous volumes instead of retrieving data from anonymous containers.

#### Stop
execute the following command in the root directory of this project:
```sh
docker-compose down -v
```
Note: The command above will remove all declared volumes in the `volumes` section of the docker-compose.yml

#### Accesses

|system|username|password|URL|
|:----:|:------:|:------:|:-:|
|IndluxDB|init_user|init_password|`http://localhos:8086`|
|Grafana|admin|admin|`http://localhost:3000`|

## Project status
The tool stack is tested with the following versions:
|Tool|Version|
|----|-------|
|docker|23.0.1, build a5ee5b1|
|docker-compose|1.28.4, build cabd5cfb|

## Experiments
### Retention of aggregated data
#### Goal
- free up disk space
- keep aggregated data

##### Prerequisites
- create a separate bucket to store the aggregated data
- create the script that ensures the
  - aggregation
  - storing data in separate bucket
